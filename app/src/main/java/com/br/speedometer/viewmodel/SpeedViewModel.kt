package com.br.speedometer.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.br.speedometer.model.SpeedDataModel
import com.br.speedometer.repository.SpeedServiceRepo
import io.reactivex.android.schedulers.AndroidSchedulers

class SpeedViewModel: ViewModel() {

    var speedDataModelResponse: MutableLiveData<SpeedDataModel>? = MutableLiveData()
    private var currentSpeed = 0
    private var suggestedSpeed = 80

    fun getCurrentSpeedData(){
        SpeedServiceRepo.getSpeedDataResponse(0, 150, 1)?.observeOn(AndroidSchedulers.mainThread())?.subscribe{
            currentSpeed = it
            speedDataModelResponse?.value = SpeedDataModel(it, suggestedSpeed)
        }
    }

    fun getSuggestedSpeedData(){
        SpeedServiceRepo.getSpeedDataResponse(80, 150, 20)?.observeOn(AndroidSchedulers.mainThread())?.subscribe{
            suggestedSpeed = it
            speedDataModelResponse?.value = SpeedDataModel(currentSpeed, it)
        }
    }
}