package com.br.speedometer.repository

import com.br.speedometer.getRandomSpeedObservable
import io.reactivex.Observable

object SpeedServiceRepo {
    fun getSpeedDataResponse(start: Int, end: Int, delay: Long): Observable<Int>? {
        return  getRandomSpeedObservable(start, end, delay)
    }
}