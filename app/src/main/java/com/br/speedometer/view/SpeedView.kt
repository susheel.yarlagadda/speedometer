package com.br.speedometer.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.br.speedometer.viewmodel.SpeedViewModel
import com.br.speedometer.databinding.ActivitySpeedBinding
import com.br.speedometer.model.SpeedDataModel

class SpeedView : AppCompatActivity() {
    private lateinit var binding: ActivitySpeedBinding
    private lateinit var viewModel: SpeedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySpeedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(SpeedViewModel::class.java)
        viewModel.getCurrentSpeedData()
        viewModel.getSuggestedSpeedData()
        viewModel.speedDataModelResponse?.observe(this, {
            setUIContent(it)
        })
    }

    private fun setUIContent(speed: SpeedDataModel?) {
        speed?.let { speedDataModel ->
            if (speedDataModel.currentSpeed > speedDataModel.suggestedSpeed) {
                binding.speedWarningImg.visibility = View.VISIBLE
                binding.suggestedSpeed.visibility = View.VISIBLE
                binding.suggestedSpeedTitle.visibility = View.VISIBLE

                binding.suggestedSpeed.text = speedDataModel.suggestedSpeed.toString()
            } else {
                binding.speedWarningImg.visibility = View.GONE
                binding.suggestedSpeed.visibility = View.GONE
                binding.suggestedSpeedTitle.visibility = View.GONE
            }

            binding.speed.text = speedDataModel.currentSpeed.toString()
        }.run {

        }
    }
}