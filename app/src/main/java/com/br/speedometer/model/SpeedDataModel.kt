package com.br.speedometer.model

class SpeedDataModel(
    val currentSpeed: Int = 0,
    val suggestedSpeed: Int = 0
)