package com.br.speedometer

import io.reactivex.Observable
import java.util.*
import java.util.concurrent.TimeUnit

fun getRandomSpeedObservable(start: Int, end: Int, delay: Long): Observable<Int>? {
    return Observable.fromCallable {
        val r = Random()
        r.nextInt(end) + start
    }.delay(delay, TimeUnit.SECONDS)
        .repeat()
        .share()
}